# Golang

This image contain only the `centos-7` and `centos-8` variants for [Golang](https://hub.docker.com/_/golang) image

`centos` variant lead to `centos-7` variant at this time.

## Usage

```
docker run -it registry.gitlab.com/nexylan/docker/golang:1.17.1-centos
```

## Supported tags

- `X-centos`, `X-centos-7`
- `X-centos-8`
- `X-rockylinux`, `X-rockylinux-8`, `X-rockylinux-9`

> X is the Golang version you want ( ex: `1.17.1` )
