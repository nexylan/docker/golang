#!/bin/bash

# shellcheck disable=SC2001
CI_ENVIRONMENT_HOST=$(echo "${CI_ENVIRONMENT_URL}" | sed 's~http[s]*://~~g')
export CI_ENVIRONMENT_HOST

if [ -z "${IMAGE_NAME}" ]; then
	export IMAGE_NAME=${CI_REGISTRY_IMAGE:-${IMAGE_NAME}}
fi

versions=$(cat "versions.txt")

for version in ${versions}; do
	export IMAGE_TAG=${version}

	if [[ -n "${CI_COMMIT_REF_SLUG}" ]]; then
		export BRANCH="-${CI_COMMIT_REF_SLUG:-latest}"
	fi

	# Don't make `branch suffix` when generating a tag
	if [[ -n "${CI_COMMIT_TAG}" ]]; then
		export BRANCH=""
	fi

	docker-compose build
	docker-compose push
done
