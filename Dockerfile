# @see https://github.com/moby/moby/issues/34482
ARG GOLANG_VERSION=latest
ARG DISTRO_VERSION=7
ARG RHEL_DISTRO_NAME=centos

FROM golang:${GOLANG_VERSION} as golang-current-version

FROM ${RHEL_DISTRO_NAME}:${DISTRO_VERSION}
ENV PATH /usr/local/go/bin:$PATH
COPY --from=golang-current-version /usr/local/go /usr/local/go
RUN sed -i s/mirror.centos.org/vault.centos.org/g /etc/yum.repos.d/*.repo
RUN sed -i s/^#.*baseurl=http/baseurl=http/g /etc/yum.repos.d/*.repo
RUN sed -i s/^mirrorlist=http/#mirrorlist=http/g /etc/yum.repos.d/*.repo
